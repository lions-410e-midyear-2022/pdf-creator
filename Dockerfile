FROM registry.gitlab.com/kimvanwyk/kppe:latest

USER root
COPY template.txt /templates
COPY *.png /templates/
RUN chown appuser:appuser /templates/*
USER appuser

ENTRYPOINT ["/usr/bin/python3", "/app/kppe.py", "build", "--abbreviations-dir", "/abbreviations", "--templates-dir", "/templates", "template"]
